<?php

/* index.tpl */
class __TwigTemplate_f9743dad417babaf36726b076d73b0358b8e619e51434988fb1a1f55e7175b79 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
";
        // line 3
        try {
            $this->env->loadTemplate("header.tpl")->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 4
        echo "<div class=\"row\">
    <div class=\"col-md-12\">
        ";
        // line 6
        $template = $this->env->resolveTemplate((isset($context["tpl"]) ? $context["tpl"] : null));
        $template->display($context);
        // line 7
        echo "    </div>
</div>
";
        // line 9
        try {
            $this->env->loadTemplate("footer.tpl")->display($context);
        } catch (Twig_Error_Loader $e) {
            // ignore missing template
        }

        // line 10
        echo "</html>";
    }

    public function getTemplateName()
    {
        return "index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 10,  41 => 9,  37 => 7,  34 => 6,  30 => 4,  23 => 3,  19 => 1,);
    }
}
