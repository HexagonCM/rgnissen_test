<?php

/* header.tpl */
class __TwigTemplate_224f3a844a15f29c1a5018d6d57cb4fd99699747cc894f5c0324ccf849e2be4d extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- <header> -->
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">

    <title>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["header_additions"]) ? $context["header_additions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["header_add"]) {
            // line 12
            echo "        ";
            echo twig_escape_filter($this->env, (isset($context["header_add"]) ? $context["header_add"] : null), "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header_add'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "
    <script src=\"media/js/jquery-2.1.0.min.js\" type=\"text/javascript\"></script>
    <script src=\"media/js/bootstrap.min.js\" type=\"text/javascript\"></script>
    <script src=\"media/js/bootstrap3-typeahead.min.js\" type=\"text/javascript\"></script>
    <script src=\"media/js/functions.js\" type=\"text/javascript\"></script>
    <script src=\"media/js/initialize.js\" type=\"text/javascript\"></script>

    ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["js"]) ? $context["js"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ele"]) {
            // line 22
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["ele"]) ? $context["ele"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ele'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
    <link href=\"media/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link href=\"media/css/bootstrap-theme.min.css\" rel=\"stylesheet\">
    <link href=\"media/css/style.css\" rel=\"stylesheet\">
    <link href=\"media/css/font-awesome.min.css\" rel=\"stylesheet\">

    ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["css"]) ? $context["css"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ele"]) {
            // line 31
            echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["ele"]) ? $context["ele"] : null), "html", null, true);
            echo "\"/>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ele'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
    <!--[if lt IE 9]>
    <script src=\"media/js/html5.min.js\"></script>
    <![endif]-->
</head>
<body>
<div class=\"container\">
<!-- <navbar> -->

<nav class=\"navbar navbar-default\" role=\"navigation\">
    <div class=\"container-fluid\">
        <div class=\"navbar-header\">
            <img class=\"pull-left header-icon\"
                 src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "getOption", array(0 => "logo_url", 1 => "media/img/icon-default.png"), "method"), "html", null, true);
        echo "\"
                 alt=\"logo\"/>
            <a class=\"navbar-brand\" href=\"./\">";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</a>
        </div>

        <ul class=\"nav navbar-nav navbar-right\">
            <li>
                <!-- Online status -->
                ";
        // line 54
        if ($this->getAttribute((isset($context["ServerStatistic"]) ? $context["ServerStatistic"] : null), "getStatus")) {
            // line 55
            echo "                    <span class=\"btn btn-success disabled navbar-btn\">";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("online")), "html", null, true);
            echo "</span>
                ";
        } else {
            // line 57
            echo "                    <span class=\"btn btn-danger disabled navbar-btn\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("offline"), "html", null, true);
            echo "</span>
                ";
        }
        // line 59
        echo "            </li>
            <li class=\"divider-vertical\"></li>
            <li>
                <!-- Search form -->
                <form class=\"navbar-form\" method=\"get\">
                    <input type=\"hidden\" name=\"page\" value=\"player\">

                    <div class=\"form-group\">
                        <input type=\"text\" name=\"name\" class=\"form-control\" placeholder=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("player"), "html", null, true);
        echo "\" id=\"playerSearch\"
                               autocomplete=\"off\">
                        <input type=\"hidden\" value=\"0\" name=\"id\" id=\"playerSearchID\"/>
                        <button class=\"btn btn-default\" value=\"1\" type=\"submit\">";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("search"), "html", null, true);
        echo "</button>
                    </div>
                </form>
            </li>
        </ul>
    </div>
</nav>

<!-- </navbar> -->

<!-- </header> -->";
    }

    public function getTemplateName()
    {
        return "header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 57,  121 => 55,  110 => 48,  90 => 33,  159 => 61,  147 => 54,  135 => 47,  119 => 54,  112 => 36,  89 => 24,  86 => 23,  80 => 21,  74 => 19,  68 => 17,  65 => 16,  56 => 21,  47 => 14,  29 => 4,  22 => 2,  102 => 39,  98 => 38,  78 => 26,  66 => 20,  62 => 15,  58 => 17,  33 => 7,  137 => 51,  124 => 43,  114 => 39,  104 => 34,  94 => 27,  85 => 25,  54 => 16,  41 => 8,  36 => 8,  32 => 5,  21 => 2,  155 => 59,  149 => 70,  143 => 67,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 22,  76 => 27,  72 => 23,  46 => 11,  35 => 6,  28 => 5,  87 => 32,  81 => 31,  71 => 18,  63 => 17,  43 => 14,  59 => 14,  45 => 11,  39 => 10,  40 => 8,  27 => 4,  24 => 3,  109 => 43,  103 => 32,  100 => 32,  96 => 38,  77 => 30,  73 => 21,  69 => 24,  57 => 22,  48 => 13,  42 => 10,  38 => 12,  30 => 7,  61 => 17,  49 => 17,  37 => 8,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 59,  129 => 59,  123 => 41,  120 => 54,  116 => 45,  108 => 36,  105 => 46,  101 => 31,  95 => 45,  92 => 29,  88 => 30,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 22,  53 => 12,  50 => 11,  44 => 9,  34 => 11,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
