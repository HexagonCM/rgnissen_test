<?php

/* overview/players.tpl */
class __TwigTemplate_2809c8636103e6d62d213fe4b7fe62568974fb8fa69bdfaa35d8d5210dcf464f extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table no-border statbox-table-small\">
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-group\"></i> ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats"]) ? $context["player_stats"] : null), "online"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("cur_online"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-danger\">
                <i class=\"fa fa-star\"></i> ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats"]) ? $context["player_stats"] : null), "tracked"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 16
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("tracked")), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-success\">
                <i class=\"fa fa-signal\"></i> ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "max_players"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("max_online"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-fire\"></i> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "total_logins"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("total_logins"), "html", null, true);
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "overview/players.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 24,  45 => 16,  39 => 13,  40 => 8,  27 => 3,  24 => 2,  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 32,  69 => 20,  57 => 16,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 19,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 33,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 14,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
