<?php

/* overview/distances.tpl */
class __TwigTemplate_011cc4801a0eebb2a6985b6036f1f9a0cada3b3333f5f14e54344d1549269544 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table no-border statbox-table-small\">
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-globe\"></i> ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "total"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("total"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-exchange\"></i> ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "foot"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 16
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("by_foot")), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-shopping-cart\"></i> ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "minecart"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 24
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("by_minecart")), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-tablet\"></i> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "boat"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 32
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("by_boat")), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-umbrella\"></i> ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "swim"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("swum"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-fighter-jet\"></i> ";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance_stats"]) ? $context["distance_stats"] : null), "flight"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("flight"), "html", null, true);
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "overview/distances.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 40,  81 => 37,  71 => 30,  63 => 25,  43 => 14,  59 => 24,  45 => 16,  39 => 13,  40 => 8,  27 => 3,  24 => 2,  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 32,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 17,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 48,  95 => 45,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 14,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
