<?php

/* overview.tpl */
class __TwigTemplate_a4ee7f459f71adda9da0e1644bf7ea4cc94096c564012137be917b146d918ac4 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- <overview> -->
<div class=\"row quick-info\">
    ";
        // line 3
        $template = $this->env->resolveTemplate((isset($context["headbar"]) ? $context["headbar"] : null));
        $template->display($context);
        // line 4
        echo "</div>
<div class=\"row\">
    <div class=\"container col-md-3\">
        ";
        // line 7
        $template = $this->env->resolveTemplate((isset($context["sidebar"]) ? $context["sidebar"] : null));
        $template->display($context);
        // line 8
        echo "    </div>

    <div class=\"col-md-9\" id=\"page-body\">
        <!-- <dashboard> -->
        <section id=\"dashboard\">
            <div class=\"row\">
                <div class=\"col-md-8\" id=\"module-online-players\">
                    <div class=\"well well-sm module module-big\">
                        <h3>";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("online_players"), "html", null, true);
        echo "</h3>

                        <div class=\"online-players\">
                            ";
        // line 19
        $template = $this->env->resolveTemplate((isset($context["online_players"]) ? $context["online_players"] : null));
        $template->display($context);
        // line 20
        echo "                        </div>
                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"well well-sm module module-small\">
                        <h3>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("server"), "html", null, true);
        echo "</h3>
                        ";
        // line 26
        $template = $this->env->resolveTemplate((isset($context["server_stats"]) ? $context["server_stats"] : null));
        $template->display($context);
        // line 27
        echo "                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <div class=\"well well-sm module module-small\">
                        <h3>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("players"), "html", null, true);
        echo "</h3>
                        ";
        // line 34
        $template = $this->env->resolveTemplate((isset($context["players"]) ? $context["players"] : null));
        $template->display($context);
        // line 35
        echo "                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"well well-sm module module-small\">
                        <h3>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("blocks"), "html", null, true);
        echo "</h3>
                        ";
        // line 40
        $template = $this->env->resolveTemplate((isset($context["blocks"]) ? $context["blocks"] : null));
        $template->display($context);
        // line 41
        echo "                    </div>
                </div>
                <div class=\"col-md-4\">
                    <div class=\"well well-sm module module-small\">
                        <h3>";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("items"), "html", null, true);
        echo "</h3>
                        ";
        // line 46
        $template = $this->env->resolveTemplate((isset($context["items"]) ? $context["items"] : null));
        $template->display($context);
        // line 47
        echo "                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-4\">
                    <div class=\"well well-sm module module-small\">
                        <h3>";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("distances"), "html", null, true);
        echo "</h3>
                        ";
        // line 54
        $template = $this->env->resolveTemplate((isset($context["distances"]) ? $context["distances"] : null));
        $template->display($context);
        // line 55
        echo "                    </div>
                </div>
                <div class=\"col-md-8\">
                    <div class=\"well well-sm module module-big\">
                        <h3>";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("deaths"), "html", null, true);
        echo "</h3>
                        ";
        // line 60
        $template = $this->env->resolveTemplate((isset($context["deaths"]) ? $context["deaths"] : null));
        $template->display($context);
        // line 61
        echo "                    </div>
                </div>
            </div>
        </section>
        <!-- </dashboard> -->

        <!-- <players> -->
        <section id=\"players\">
            <h1><i class=\"fa fa-group\"></i> ";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("players"), "html", null, true);
        echo "
                <small>";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("tracked_on_server"), "html", null, true);
        echo "</small>
            </h1>

            <div data-mod=\"players\" id=\"playersBlock\" class=\"table-responsive\">
                ";
        // line 74
        $template = $this->env->resolveTemplate((isset($context["total_players"]) ? $context["total_players"] : null));
        $template->display($context);
        // line 75
        echo "            </div>
        </section>
        <!-- </players> -->

        <!-- <world> -->
        <section id=\"world\">
            <div class=\"row\" id=\"blocks\">
                <div class=\"col-md-12\">
                    <h1><i class=\"fa fa-picture-o fa-lg\"></i> ";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("blocks"), "html", null, true);
        echo "</h1>

                    <div class=\"well custom-well paginator table-responsive\" data-mod=\"total_blocks\" id=\"worldBlocks\">
                        ";
        // line 86
        $template = $this->env->resolveTemplate((isset($context["total_blocks"]) ? $context["total_blocks"] : null));
        $template->display($context);
        // line 87
        echo "                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\" id=\"items\">
                    <h1><i class=\"fa fa-legal fa-lg\"></i> ";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("items"), "html", null, true);
        echo "</h1>

                    <div class=\"well custom-well table-responsive\" data-mod=\"total_items\" id=\"worldItems\">
                        ";
        // line 95
        $template = $this->env->resolveTemplate((isset($context["total_items"]) ? $context["total_items"] : null));
        $template->display($context);
        // line 96
        echo "                    </div>
                </div>
            </div>
        </section>
        <!-- </world> -->

        <!-- <deaths> -->
        <section id=\"deaths\">
            <h1><i class=\"fa fa-tint fa-lg\"></i> ";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("death_log"), "html", null, true);
        echo "
                <small>";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("pvp"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("pve"), "html", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("evp"), "html", null, true);
        echo " kills</small>
            </h1>
            <div class=\"well custom-well\" style=\"padding: 10px;\" id=\"deathsBlock\">
                ";
        // line 108
        $template = $this->env->resolveTemplate((isset($context["death_log"]) ? $context["death_log"] : null));
        $template->display($context);
        // line 109
        echo "            </div>
        </section>
        <!-- </deaths> -->
    </div>
</div>
<!-- </overview> -->";
    }

    public function getTemplateName()
    {
        return "overview.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 35,  79 => 34,  75 => 33,  67 => 27,  64 => 26,  60 => 25,  53 => 20,  50 => 19,  44 => 16,  34 => 8,  31 => 7,  26 => 4,  23 => 3,  19 => 1,);
    }
}
