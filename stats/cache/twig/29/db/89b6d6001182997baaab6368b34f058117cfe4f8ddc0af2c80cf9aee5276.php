<?php

/* overview/online_players.tpl */
class __TwigTemplate_29db89b6d6001182997baaab6368b34f058117cfe4f8ddc0af2c80cf9aee5276 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["players_online"]) ? $context["players_online"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
            // line 2
            echo "    <div class=\"online-player-heads\">
        <a href=\"?page=player&id=";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlayerId"), "html", null, true);
            echo "\">
            ";
            // line 4
            echo $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlayerHead", array(0 => 64, 1 => "img-thumbnail", 2 => true), "method");
            echo "
        </a>
    </div>
";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 8
            echo "    <div class='force-center'><em>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("no_players_online"), "html", null, true);
            echo "</em></div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "overview/online_players.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  27 => 3,  24 => 2,  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 22,  69 => 20,  57 => 16,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 19,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 33,  67 => 19,  64 => 18,  60 => 25,  53 => 15,  50 => 14,  44 => 16,  34 => 8,  31 => 4,  26 => 4,  23 => 3,  19 => 1,);
    }
}
