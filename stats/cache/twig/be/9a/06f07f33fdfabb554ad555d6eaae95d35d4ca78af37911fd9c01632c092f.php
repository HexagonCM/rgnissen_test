<?php

/* overview/sidebar.tpl */
class __TwigTemplate_be9a06f07f33fdfabb554ad555d6eaae95d35d4ca78af37911fd9c01632c092f extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- <navigation> -->
<div id=\"left-menu\">
    <div class=\"nav nav-sidebar well\">
        <ul class=\"nav nav-list\">
            <li><a href=\"#dashboard\"><i class=\"fa fa-tasks fa-fw\"></i> ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("dashboard"), "html", null, true);
        echo "</a></li>
            <li class=\"divider\"></li>
            <li><a href=\"#players\"><i class=\"fa fa-group fa-fw\"></i> ";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("players"), "html", null, true);
        echo "</a></li>
            <li><a href=\"#blocks\"><i class=\"fa fa-picture-o fa-fw\"></i> ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("blocks"), "html", null, true);
        echo "</a></li>
            <li><a href=\"#items\"><i class=\"fa fa-legal fa-fw\"></i> ";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("items"), "html", null, true);
        echo "</a></li>
            <li><a href=\"#deaths\"><i class=\"fa fa-tint fa-fw\"></i> ";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("death_log"), "html", null, true);
        echo "</a></li>
        </ul>
    </div>
    ";
        // line 13
        if (((twig_constant("DB_TYPE") == "default") && (isset($context["multi"]) ? $context["multi"] : null))) {
            // line 14
            echo "        <div class=\"well\">
            <h4>";
            // line 15
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("other_servers"), "html", null, true);
            echo "</h4>
        ";
            // line 16
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["multi"]) ? $context["multi"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["server"]) {
                // line 17
                echo "            ";
                $context["info"] = $this->getAttribute((isset($context["info_ar"]) ? $context["info_ar"] : null), $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "slug"), array(), "array");
                // line 18
                echo "
            ";
                // line 19
                if (($this->getAttribute((isset($context["info"]) ? $context["info"] : null), "current_uptime") > 0)) {
                    // line 20
                    echo "                <i class=\"fa fa-circle text-success\"></i>
            ";
                } else {
                    // line 22
                    echo "                <i class=\"fa fa-circle text-danger\"></i>
            ";
                }
                // line 24
                echo "            <a href=\"?server=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "slug"), "html", null, true);
                echo "\">
                ";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["server"]) ? $context["server"] : null), "name"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "online_players"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["info"]) ? $context["info"] : null), "players_allowed"), "html", null, true);
                echo "
            </a>
            <br>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['server'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "        </div>
    ";
        } elseif ((twig_constant("DB_TYPE") != "default")) {
            // line 31
            echo "        <div class=\"well\">
            <a href=\"?server=default\"><h4><i class=\"fa fa-reply\"></i> ";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("back_main_server"), "html", null, true);
            echo "</h4></a>
        </div>
    ";
        }
        // line 35
        echo "</div>
<!-- </navigation> -->";
    }

    public function getTemplateName()
    {
        return "overview/sidebar.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 22,  69 => 20,  57 => 16,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 19,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 33,  67 => 19,  64 => 18,  60 => 25,  53 => 15,  50 => 14,  44 => 16,  34 => 8,  31 => 7,  26 => 4,  23 => 3,  19 => 1,);
    }
}
