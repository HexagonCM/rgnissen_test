<?php

/* footer.tpl */
class __TwigTemplate_86fecc192ccb8a421074a9770dfa062345d0cf3d341b4ae7a86977b9174d9912 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- <footer> -->
<footer>
    <div class=\"row footer-block\">
        <div class=\"col-md-6\">
            <a href=\"http://dev.bukkit.org/server-mods/statistics/\">
                <img class=\"img-responsive\" src=\"media/img/plugin_logo.png\" alt=\"Statistics\"/>
            </a>
        </div>
        <div class=\"col-md-4 col-md-offset-2\" style=\"text-align: right;\">
            <p>
                ";
        // line 11
        echo twig_escape_filter($this->env, twig_constant("VERSION"), "html", null, true);
        echo "-db";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "getOption", array(0 => "version"), "method"), "html", null, true);
        echo "
                <br>
                <small id=\"execution_time\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("exec_time", $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "getExecTime")), "html", null, true);
        echo "
                </small>
                <br>
                &copy; ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter("now", "Y"), "html", null, true);
        echo " Statistics -
                <a href=\"?page=admin\">
                    <small>Admin</small>
                </a>
            </p>
        </div>
    </div>
</footer>
<!-- </footer> -->
</div>
</body>";
    }

    public function getTemplateName()
    {
        return "footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 57,  121 => 55,  110 => 48,  90 => 33,  159 => 61,  147 => 54,  135 => 47,  119 => 54,  112 => 36,  89 => 24,  86 => 23,  80 => 21,  74 => 19,  68 => 17,  65 => 16,  56 => 21,  47 => 14,  29 => 4,  22 => 2,  102 => 39,  98 => 38,  78 => 26,  66 => 20,  62 => 15,  58 => 17,  33 => 7,  137 => 51,  124 => 43,  114 => 39,  104 => 34,  94 => 27,  85 => 25,  54 => 16,  41 => 8,  36 => 8,  32 => 5,  21 => 2,  155 => 59,  149 => 70,  143 => 67,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 22,  76 => 27,  72 => 23,  46 => 11,  35 => 6,  28 => 5,  87 => 32,  81 => 31,  71 => 18,  63 => 17,  43 => 14,  59 => 14,  45 => 17,  39 => 14,  40 => 8,  27 => 4,  24 => 3,  109 => 43,  103 => 32,  100 => 32,  96 => 38,  77 => 30,  73 => 21,  69 => 24,  57 => 22,  48 => 13,  42 => 10,  38 => 12,  30 => 7,  61 => 17,  49 => 17,  37 => 8,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 59,  129 => 59,  123 => 41,  120 => 54,  116 => 45,  108 => 36,  105 => 46,  101 => 31,  95 => 45,  92 => 29,  88 => 30,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 22,  53 => 12,  50 => 11,  44 => 9,  34 => 11,  31 => 11,  26 => 4,  23 => 3,  19 => 1,);
    }
}
