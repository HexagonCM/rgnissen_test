<?php

/* player.tpl */
class __TwigTemplate_46e8eedf9325bf1b703f1d79de3ab2240ca5a50dc661cda52973d8d9fede5d39 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((!(null === (isset($context["player"]) ? $context["player"] : null)))) {
            // line 2
            echo "    <div class=\"row\">
        <div class=\" col-md-7\">
            <div class=\"well well-sm\"
                 style=\"position: relative;\">
                <div class=\"player-top-left-info\">
                    ";
            // line 7
            if ($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getOnline")) {
                // line 8
                echo "                        <h4 class=\"player-top-left-info-label\">
                            <span class='label label-success'>In-Game</span>
                        </h4>
                    ";
            } else {
                // line 12
                echo "                        <h4 class=\"player-top-left-info-label\">
                            <span class='label label-danger'>Offline</span>
                        </h4>
                    ";
            }
            // line 16
            echo "                    ";
            if ($this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getIsOp")) {
                // line 17
                echo "                        <h4 class=\"player-top-left-info-label\">
                            <span class=\"label label-info\">OP</span>
                        </h4>
                    ";
            }
            // line 21
            echo "                </div>
                <h1>
                    ";
            // line 23
            echo $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlayerHead", array(0 => 64, 1 => "img-thumbnail"), "method");
            echo "
                    ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getName"), "html", null, true);
            echo "
                </h1>

                <div class=\"player-top-right-info\">
                    ";
            // line 28
            if ($this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getIsBanned")) {
                // line 29
                echo "                        <h4 class=\"player-top-right-info-label\">
                            <span class=\"label label-danger\">
                                <strong>banned</strong>
                            </span>
                        </h4>
                    ";
            }
            // line 35
            echo "                    ";
            if (($this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getGamemode") > 0)) {
                // line 36
                echo "                        <h4 class=\"player-top-right-info-label\">
                            <span class=\"label label-warning\">
                                ";
                // line 38
                if (($this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getGamemode") == 1)) {
                    // line 39
                    echo "                                    <strong>creative</strong>
                                ";
                } else {
                    // line 41
                    echo "                                    <strong>adventure</strong>
                                ";
                }
                // line 43
                echo "                            </span>
                        </h4>
                    ";
            }
            // line 46
            echo "                </div>

                <div class=\"bar-container\">
                    <div class=\"xpbar-cur\">
                        <strong>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getExpLevel"), "html", null, true);
            echo "</strong>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-md-12\">
                            <div class=\"armor-bar clearfix\">
                                ";
            // line 55
            echo $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getArmorBar");
            echo "&nbsp;
                            </div>
                        </div>
                    </div>
                    <div class=\"row\" id=\"playerhead-bars\">
                        <div class=\"col-md-6\">
                            <div class=\"heart-bar clearfix\">
                                ";
            // line 62
            echo $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getHealthBar");
            echo "
                            </div>
                        </div>

                        <div class=\"col-md-6\">
                            <div class=\"hunger-bar clearfix\">
                                ";
            // line 68
            echo $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getFoodBar");
            echo "
                            </div>
                        </div>
                    </div>

                    <div class=\"force-center xpbar-container\">
                        ";
            // line 74
            echo $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "getXPBar");
            echo "
                    </div>
                </div>

                <div class=\"player-effects\">
                    ";
            // line 79
            if ((isset($context["inv"]) ? $context["inv"] : null)) {
                // line 80
                echo "                        ";
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printEffects");
                echo "
                    ";
            }
            // line 82
            echo "                </div>
            </div>
        </div>
        <div class=\"col-md-5\">
            ";
            // line 86
            if ((isset($context["inv"]) ? $context["inv"] : null)) {
                // line 87
                echo "                <div class=\"player-inv pull-right hidden-xs\">
                    <div class=\"player-inv-row clearfix\">
                        ";
                // line 89
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printRowOne");
                echo "
                    </div>
                    <div class=\"player-inv-row clearfix\">
                        ";
                // line 92
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printRowTwo");
                echo "
                    </div>
                    <div class=\"player-inv-row clearfix\">
                        ";
                // line 95
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printRowThree");
                echo "
                    </div>
                    <div class=\"player-inv-hotbar clearfix\">
                        ";
                // line 98
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printHotbar");
                echo "
                    </div>
                </div>
                <div class=\"player-armor pull-right hidden-xs\">
                    ";
                // line 102
                echo $this->getAttribute((isset($context["inv"]) ? $context["inv"] : null), "printArmor");
                echo "
                </div>
            ";
            }
            // line 105
            echo "        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-3 center\">
            ";
            // line 109
            if ((!(null === $this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by")))) {
                // line 110
                echo "                ";
                $context["pvp_player"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by"), "createPlayer", array(0 => "player_id"), "method");
                // line 111
                echo "            ";
            } else {
                // line 112
                echo "                ";
                $context["pvp_player"] = $this->getAttribute($this->env->getExtension('Statistics')->Player(), "setName", array(0 => "none"), "method");
                // line 113
                echo "            ";
            }
            // line 114
            echo "            ";
            echo $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getPlayerHead", array(0 => 64, 1 => "img-thumbnail"), "method");
            echo "
            <h4 class=\"well well-sm center\">
                ";
            // line 116
            if (($this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName") != "none")) {
                // line 117
                echo "                    <a href=\"?page=player&id=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getPlayerId"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName"), "html", null, true);
                echo "</a>
                ";
            } else {
                // line 119
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName"), "html", null, true);
                echo "
                ";
            }
            // line 121
            echo "                <br>
                <small>Arch Nemesis</small>
            </h4>
        </div>
        <div class=\"col-md-3 center\">
            ";
            // line 126
            if ((!(null === $this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed")))) {
                // line 127
                echo "                ";
                $context["pvp_player"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed"), "createPlayer", array(0 => "victim_id"), "method");
                // line 128
                echo "            ";
            } else {
                // line 129
                echo "                ";
                $context["pvp_player"] = $this->getAttribute($this->env->getExtension('Statistics')->Player(), "setName", array(0 => "none"), "method");
                // line 130
                echo "            ";
            }
            // line 131
            echo "            ";
            echo $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getPlayerHead", array(0 => 64, 1 => "img-thumbnail"), "method");
            echo "
            <h4 class=\"well well-sm center\">
                ";
            // line 133
            if (($this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName") != "none")) {
                // line 134
                echo "                    <a href=\"?page=player&id=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getPlayerId"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName"), "html", null, true);
                echo "</a>
                ";
            } else {
                // line 136
                echo "                    ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pvp_player"]) ? $context["pvp_player"] : null), "getName"), "html", null, true);
                echo "
                ";
            }
            // line 138
            echo "                <br>
                <small>Most killed</small>
            </h4>
        </div>
        <div class=\"col-md-3 center\">
            ";
            // line 143
            if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_destroyed")) {
                // line 144
                echo "                ";
                $context["block"] = $this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_destroyed"), "createMaterial");
                // line 145
                echo "            ";
            } else {
                // line 146
                echo "                ";
                $context["block"] = $this->env->getExtension('Statistics')->Material("-1:0");
                // line 147
                echo "            ";
            }
            // line 148
            echo "            ";
            echo $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getImage", array(0 => 64, 1 => "img-thumbnail"), "method");
            echo "
            <h4 class=\"well well-sm center\">
                ";
            // line 150
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getName"), "html", null, true);
            echo "
                <br>
                <small>Most broken</small>
            </h4>
        </div>
        <div class=\"col-md-3 center\">
            ";
            // line 156
            if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_placed")) {
                // line 157
                echo "                ";
                $context["block"] = $this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_placed"), "createMaterial");
                // line 158
                echo "            ";
            } else {
                // line 159
                echo "                ";
                $context["block"] = $this->env->getExtension('Statistics')->Material("-1:0");
                // line 160
                echo "            ";
            }
            // line 161
            echo "            ";
            echo $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getImage", array(0 => 64, 1 => "img-thumbnail"), "method");
            echo "
            <h4 class=\"well well-sm center\">
                ";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getName"), "html", null, true);
            echo "
                <br>
                <small>Most placed</small>
            </h4>
        </div>
    </div>
    <h2>General Statistics</h2>

    <div class=\"row col-wrap-320\">
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>Blocks</h3>

                <p>
                    <strong>Total Placed:</strong>
                    ";
            // line 178
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "placed")), "html", null, true);
            echo " Blocks
                </p>

                <p>
                    <strong>Most Placed:</strong>
                    ";
            // line 183
            if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_placed")) {
                // line 184
                echo "                        ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_placed"), "createMaterial"), "getImage", array(0 => 32, 1 => null, 2 => true), "method");
                echo "
                        ";
                // line 185
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_placed"), "preparePlaced"), "html", null, true);
                echo "
                    ";
            } else {
                // line 187
                echo "                        <em>none</em>
                    ";
            }
            // line 189
            echo "                </p>

                <p>
                    <strong>Total Destroyed:</strong>
                    ";
            // line 193
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "destroyed")), "html", null, true);
            echo " Blocks
                </p>

                <p>
                    <strong>Most Destroyed:</strong>
                    ";
            // line 198
            if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_destroyed")) {
                // line 199
                echo "                        ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_destroyed"), "createMaterial"), "getImage", array(0 => 32, 1 => null, 2 => true), "method");
                echo "
                        ";
                // line 200
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "most_destroyed"), "prepareDestroyed"), "html", null, true);
                echo "
                    ";
            } else {
                // line 202
                echo "                        <em>none</em>
                    ";
            }
            // line 204
            echo "                </p>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>Items</h3>

                <p>
                    <strong>Total Picked Up:</strong>
                    ";
            // line 213
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "picked")), "html", null, true);
            echo " Items
                </p>

                <p>
                    <strong>Most Picked Up:</strong>
                    ";
            // line 218
            if ($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_picked")) {
                // line 219
                echo "                        ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_picked"), "createMaterial"), "getImage", array(0 => 32, 1 => null, 2 => true), "method");
                echo "
                        ";
                // line 220
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_picked"), "preparePickedUp"), "html", null, true);
                echo "
                    ";
            } else {
                // line 222
                echo "                        <em>none</em>
                    ";
            }
            // line 224
            echo "                </p>

                <p>
                    <strong>Total Dropped:</strong>
                    ";
            // line 228
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "dropped")), "html", null, true);
            echo " Items
                </p>

                <p>
                    <strong>Most Dropped:</strong>
                    ";
            // line 233
            if ($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_dropped")) {
                // line 234
                echo "                        ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_dropped"), "createMaterial"), "getImage", array(0 => 32, 1 => null, 2 => true), "method");
                echo "
                        ";
                // line 235
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["items"]) ? $context["items"] : null), "most_dropped"), "prepareDropped"), "html", null, true);
                echo "
                    ";
            } else {
                // line 237
                echo "                        <em>none</em>
                    ";
            }
            // line 239
            echo "                </p>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>Distances</h3>

                <p>
                    <strong>Travelled:</strong>
                    ";
            // line 248
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareTotal"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Walked:</strong>
                    ";
            // line 253
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareFoot"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Minecarted:</strong>
                    ";
            // line 258
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareMinecart"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Boated:</strong>
                    ";
            // line 263
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareBoat"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Ridden:</strong>
                    ";
            // line 268
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareRide"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Swum:</strong>
                    ";
            // line 273
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareSwim"), "html", null, true);
            echo " meters
                </p>

                <p>
                    <strong>Flight:</strong>
                    ";
            // line 278
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["distance"]) ? $context["distance"] : null), "prepareFlight"), "html", null, true);
            echo " meters
                </p>
            </div>
        </div>
    </div>
    <div class=\"row col-wrap-220\">
        <div class=\"col-md-8\">
            <div class=\"well well-sm table-responsive\">
                <h3>Miscellaneous</h3>
                <table class=\"table table-condensed no-border\">
                    <tr>
                        <td>
                            <strong>Total XP:</strong>
                        </td>
                        <td>
                            ";
            // line 293
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareExpTotal"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Times kicked:</strong>
                        </td>
                        <td>
                            ";
            // line 299
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareTimesKicked"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Eggs thrown:</strong>
                        </td>
                        <td>
                            ";
            // line 305
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareEggsThrown"), "html", null, true);
            echo "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Food eaten:</strong>
                        </td>
                        <td>
                            ";
            // line 313
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareFoodEaten"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Arrows shot:</strong>
                        </td>
                        <td>
                            ";
            // line 319
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareArrowsShot"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Damage taken:</strong>
                        </td>
                        <td>
                            ";
            // line 325
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareDamageTaken"), "html", null, true);
            echo "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Words said:</strong>
                        </td>
                        <td>
                            ";
            // line 333
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareWordsSaid"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Commands sent:</strong>
                        </td>
                        <td>
                            ";
            // line 339
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareCommandsSent"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Beds entered:</strong>
                        </td>
                        <td>
                            ";
            // line 345
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareBedsEntered"), "html", null, true);
            echo "
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Portals entered:</strong>
                        </td>
                        <td>
                            ";
            // line 353
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "preparePortalsEntered"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Fish caught:</strong>
                        </td>
                        <td>
                            ";
            // line 359
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareFishCaught"), "html", null, true);
            echo "
                        </td>
                        <td>
                            <strong>Times jumped:</strong>
                        </td>
                        <td>
                            ";
            // line 365
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareTimesJumped"), "html", null, true);
            echo "
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>Login statistics</h3>

                <p>
                    <strong>Joined on:</strong>
                    ";
            // line 377
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getFirstLogin")), "html", null, true);
            echo "
                </p>

                <p>
                    <strong>Last seen:</strong>
                    ";
            // line 382
            if (((!(null === $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLoginTime"))) && ($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLoginTime") != 0))) {
                // line 383
                echo "                        ";
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLoginTime")), "html", null, true);
                echo "
                    ";
            } else {
                // line 385
                echo "                        <em>never</em>
                    ";
            }
            // line 387
            echo "                </p>

                <p>
                    <strong>Playtime:</strong>
                    ";
            // line 391
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "formatSeconds", array(0 => $this->env->getExtension('Statistics')->fTimestamp($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlaytime")), 1 => false), "method"), "html", null, true);
            echo "
                </p>

                <p>
                    <strong>Longest session:</strong>
                    ";
            // line 396
            if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLongestSession") > 0)) {
                // line 397
                echo "                        ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "formatSeconds", array(0 => $this->env->getExtension('Statistics')->fTimestamp($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLongestSession")), 1 => false), "method"), "html", null, true);
                echo "
                    ";
            } else {
                // line 399
                echo "                        <em>none</em>
                    ";
            }
            // line 401
            echo "                </p>

                <p>
                    <strong>Logins:</strong>
                    ";
            // line 405
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "prepareLogins"), "html", null, true);
            echo "
                </p>
            </div>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">

                <h3>PvP</h3>

                <p>
                    <strong>Total Kills:</strong>
                    ";
            // line 418
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "kills")), "html", null, true);
            echo "
                </p>

                <p>
                    <strong>Total Deaths:</strong>
                    ";
            // line 423
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "deaths")), "html", null, true);
            echo "
                </p>

                <p>
                    <strong>Current kill streak:</strong>
                    ";
            // line 428
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareKillStreak"), "html", null, true);
            echo "
                </p>

                <p>
                    <strong>Best kill streak:</strong>
                    ";
            // line 433
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["misc"]) ? $context["misc"] : null), "prepareMaxKillStreak"), "html", null, true);
            echo "
                </p>
                ";
            // line 435
            if ($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed")) {
                // line 436
                echo "                    <br/>
                    <h4>Most killed:</h4>

                    <p>
                        ";
                // line 440
                $context["victim"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed"), "createPlayer", array(0 => "victim_id"), "method");
                // line 441
                echo "                        <a href=\"?page=player&id=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getPlayerId"), "html", null, true);
                echo "\">
                            ";
                // line 442
                echo $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getPlayerHead");
                echo "
                            ";
                // line 443
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getName"), "html", null, true);
                echo "
                        </a>
                    </p>

                    <p>
                        <strong>Kills:</strong>
                        ";
                // line 449
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed"), "getTimes")), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Used weapon:</strong>
                        ";
                // line 454
                $context["weapon"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed"), "createMaterial");
                // line 455
                echo "                        ";
                echo $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getImage");
                echo "
                        ";
                // line 456
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getName"), "html", null, true);
                echo "
                    </p>
                ";
            }
            // line 459
            echo "                ";
            if ($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by")) {
                // line 460
                echo "                    <br/>
                    <h4>Most killed by:</h4>

                    <p>
                        ";
                // line 464
                $context["killer"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by"), "createPlayer", array(0 => "player_id"), "method");
                // line 465
                echo "                        <a href=\"?page=player&id=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getPlayerId"), "html", null, true);
                echo "\">
                            ";
                // line 466
                echo $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getPlayerHead");
                echo "
                            ";
                // line 467
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getName"), "html", null, true);
                echo "
                        </a>
                    </p>

                    <p>
                        <strong>Kills:</strong>
                        ";
                // line 473
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by"), "getTimes")), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Used weapon:</strong>
                        ";
                // line 478
                $context["weapon"] = $this->getAttribute($this->getAttribute((isset($context["pvp"]) ? $context["pvp"] : null), "most_killed_by"), "createMaterial");
                // line 479
                echo "                        ";
                echo $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getImage");
                echo "
                        ";
                // line 480
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getName"), "html", null, true);
                echo "
                    </p>
                ";
            }
            // line 483
            echo "            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>PvE</h3>

                <p>
                    <strong>Total Kills:</strong>
                    ";
            // line 491
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "kills")), "html", null, true);
            echo "
                </p>

                <p><strong>Total Deaths:</strong>
                    ";
            // line 495
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "deaths")), "html", null, true);
            echo "
                </p>
                ";
            // line 497
            if ($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed")) {
                // line 498
                echo "                    <br/>
                    <h4>Most killed:</h4>

                    <p>
                        ";
                // line 502
                $context["victim"] = $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed"), "createEntity");
                // line 503
                echo "                        ";
                echo $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getImage");
                echo "
                        ";
                // line 504
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getName"), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Kills:</strong>
                        ";
                // line 509
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed"), "prepareCreatureKilled"), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Used weapon:</strong>
                        ";
                // line 514
                $context["weapon"] = $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed"), "createMaterial");
                // line 515
                echo "                        ";
                echo $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getImage");
                echo "
                        ";
                // line 516
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getName"), "html", null, true);
                echo "
                    </p>
                ";
            }
            // line 519
            echo "                ";
            if ($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed_by")) {
                // line 520
                echo "                    <br/>
                    <h4>Most killed by:</h4>

                    <p>
                        ";
                // line 524
                $context["killer"] = $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed_by"), "createEntity");
                // line 525
                echo "                        ";
                echo $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getImage");
                echo "
                        ";
                // line 526
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getName"), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Kills:</strong>
                        ";
                // line 531
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed_by"), "preparePlayerKilled"), "html", null, true);
                echo "
                    </p>

                    <p>
                        <strong>Used weapon:</strong>
                        ";
                // line 536
                $context["weapon"] = $this->getAttribute($this->getAttribute((isset($context["pve"]) ? $context["pve"] : null), "most_killed_by"), "createMaterial");
                // line 537
                echo "                        ";
                echo $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getImage");
                echo "
                        ";
                // line 538
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["weapon"]) ? $context["weapon"] : null), "getName"), "html", null, true);
                echo "
                    </p>
                ";
            }
            // line 541
            echo "            </div>
        </div>

        <div class=\"col-md-4\">
            <div class=\"well well-sm\">
                <h3>Other</h3>
                ";
            // line 547
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["deaths"]) ? $context["deaths"] : null));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["death"]) {
                // line 548
                echo "                    <p>
                        <strong>";
                // line 549
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["death"]) ? $context["death"] : null), "getName"), "html", null, true);
                echo "</strong>
                        ";
                // line 550
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->getAttribute((isset($context["death"]) ? $context["death"] : null), "getTimes")), "html", null, true);
                echo "
                    </p>
                ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 553
                echo "                    <p><strong>This player was not killed by outside influences.</strong></p>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['death'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 555
            echo "            </div>
        </div>
    </div>
    <h2>Detailed Information</h2>

    <div class=\"row\">
        <div class=\"col-md-6\">
            <div class=\" well well-sm\">
                <h3>Blocks</h3>

                <div class=\"paginator\" data-mod=\"player_blocks\" class=\"table-responsive\">
                    ";
            // line 566
            $template = $this->env->resolveTemplate((isset($context["player_blocks"]) ? $context["player_blocks"] : null));
            $template->display($context);
            // line 567
            echo "                </div>
            </div>
        </div>

        <div class=\"col-md-6\">
            <div class=\"well well-sm\">
                <h3>Items</h3>

                <div class=\"paginator\" data-mod=\"player_items\" class=\"table-responsive\">
                    ";
            // line 576
            $template = $this->env->resolveTemplate((isset($context["player_items"]) ? $context["player_items"] : null));
            $template->display($context);
            // line 577
            echo "                </div>
            </div>
        </div>
    </div>
";
        } else {
            // line 582
            echo "    <div class=\"alert alert-block alert-danger\">
        <h3>Player not found!</h3>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "player.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1053 => 582,  1046 => 577,  1043 => 576,  1032 => 567,  1029 => 566,  1016 => 555,  1009 => 553,  1001 => 550,  997 => 549,  994 => 548,  989 => 547,  981 => 541,  975 => 538,  970 => 537,  968 => 536,  960 => 531,  952 => 526,  947 => 525,  945 => 524,  939 => 520,  936 => 519,  930 => 516,  925 => 515,  923 => 514,  915 => 509,  907 => 504,  902 => 503,  900 => 502,  894 => 498,  892 => 497,  887 => 495,  880 => 491,  870 => 483,  864 => 480,  859 => 479,  857 => 478,  849 => 473,  840 => 467,  836 => 466,  831 => 465,  829 => 464,  823 => 460,  820 => 459,  814 => 456,  809 => 455,  807 => 454,  799 => 449,  790 => 443,  786 => 442,  781 => 441,  779 => 440,  773 => 436,  771 => 435,  766 => 433,  758 => 428,  750 => 423,  742 => 418,  726 => 405,  720 => 401,  716 => 399,  710 => 397,  708 => 396,  700 => 391,  694 => 387,  690 => 385,  684 => 383,  682 => 382,  674 => 377,  659 => 365,  650 => 359,  641 => 353,  630 => 345,  621 => 339,  612 => 333,  601 => 325,  592 => 319,  583 => 313,  572 => 305,  563 => 299,  554 => 293,  536 => 278,  528 => 273,  520 => 268,  512 => 263,  504 => 258,  496 => 253,  488 => 248,  477 => 239,  473 => 237,  468 => 235,  463 => 234,  461 => 233,  453 => 228,  447 => 224,  443 => 222,  438 => 220,  433 => 219,  431 => 218,  423 => 213,  412 => 204,  408 => 202,  403 => 200,  398 => 199,  396 => 198,  388 => 193,  382 => 189,  378 => 187,  373 => 185,  368 => 184,  366 => 183,  358 => 178,  340 => 163,  334 => 161,  331 => 160,  328 => 159,  325 => 158,  322 => 157,  320 => 156,  311 => 150,  305 => 148,  302 => 147,  299 => 146,  296 => 145,  293 => 144,  291 => 143,  284 => 138,  278 => 136,  270 => 134,  268 => 133,  262 => 131,  259 => 130,  256 => 129,  253 => 128,  250 => 127,  248 => 126,  241 => 121,  235 => 119,  227 => 117,  225 => 116,  219 => 114,  216 => 113,  213 => 112,  210 => 111,  207 => 110,  205 => 109,  199 => 105,  193 => 102,  186 => 98,  180 => 95,  174 => 92,  168 => 89,  164 => 87,  162 => 86,  156 => 82,  150 => 80,  148 => 79,  140 => 74,  131 => 68,  122 => 62,  112 => 55,  104 => 50,  98 => 46,  93 => 43,  89 => 41,  85 => 39,  83 => 38,  79 => 36,  76 => 35,  68 => 29,  66 => 28,  59 => 24,  55 => 23,  51 => 21,  45 => 17,  42 => 16,  36 => 12,  30 => 8,  28 => 7,  21 => 2,  19 => 1,);
    }
}
