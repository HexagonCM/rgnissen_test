<?php

/* mod/death_log.tpl */
class __TwigTemplate_7989552a8be0d6ede9896c25869ba75097ee81e32e41a723f1b75085794647cb extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content\" data-mod=\"death_log\">
    ";
        // line 2
        if (($this->getAttribute((isset($context["death_log"]) ? $context["death_log"] : null), "countReturnedRows") != 0)) {
            // line 3
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["death_log"]) ? $context["death_log"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
                // line 4
                echo "            ";
                if ((null === $this->getAttribute((isset($context["log"]) ? $context["log"] : null), "player_killed"))) {
                    // line 5
                    echo "                ";
                    $context["killer"] = $this->env->getExtension('Statistics')->Player($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id1"));
                    // line 6
                    echo "                ";
                    $context["victim"] = $this->env->getExtension('Statistics')->Player($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id2"));
                    // line 7
                    echo "                ";
                    $context["killer_img"] = $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getPlayerHead", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                    // line 8
                    echo "                ";
                    $context["victim_img"] = $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getPlayerHead", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                    // line 9
                    echo "            ";
                } else {
                    // line 10
                    echo "                ";
                    if ($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "player_killed")) {
                        // line 11
                        echo "                    ";
                        $context["killer"] = $this->env->getExtension('Statistics')->Entity($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id1"));
                        // line 12
                        echo "                    ";
                        $context["victim"] = $this->env->getExtension('Statistics')->Player($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id2"));
                        // line 13
                        echo "                    ";
                        $context["killer_img"] = $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getImage", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                        // line 14
                        echo "                    ";
                        $context["victim_img"] = $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getPlayerHead", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                        // line 15
                        echo "                ";
                    } else {
                        // line 16
                        echo "                    ";
                        $context["killer"] = $this->env->getExtension('Statistics')->Player($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id2"));
                        // line 17
                        echo "                    ";
                        $context["victim"] = $this->env->getExtension('Statistics')->Entity($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "id1"));
                        // line 18
                        echo "                    ";
                        $context["killer_img"] = $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getPlayerHead", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                        // line 19
                        echo "                    ";
                        $context["victim_img"] = $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getImage", array(0 => 32, 1 => "img-thumbnail", 2 => true), "method");
                        // line 20
                        echo "                ";
                    }
                    // line 21
                    echo "            ";
                }
                // line 22
                echo "            ";
                $context["material"] = $this->env->getExtension('Statistics')->Material($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "material_id"));
                // line 23
                echo "            ";
                $context["time"] = $this->env->getExtension('Statistics')->fTimestamp($this->getAttribute((isset($context["log"]) ? $context["log"] : null), "time"));
                // line 24
                echo "            <div class=\"well well-sm\">
                <div class=\"row\">
                    <div class=\"col-md-3\">
                        ";
                // line 27
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter((isset($context["time"]) ? $context["time"] : null)), "html", null, true);
                echo "
                    </div>
                    <div class=\"col-md-3\">
                        <span class=\"label label-success\">
                            ";
                // line 31
                echo (isset($context["killer_img"]) ? $context["killer_img"] : null);
                echo "
                            ";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["killer"]) ? $context["killer"] : null), "getName"), "html", null, true);
                echo "
                        </span>
                    </div>
                    <div class=\"col-md-2\">
                        ";
                // line 36
                echo $this->getAttribute((isset($context["material"]) ? $context["material"] : null), "getImage", array(0 => 32, 1 => null, 2 => true), "method");
                echo "
                    </div>
                    <div class=\"col-md-4\">
                        <span class=\"label label-danger\">
                            ";
                // line 40
                echo (isset($context["victim_img"]) ? $context["victim_img"] : null);
                echo "
                            ";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["victim"]) ? $context["victim"] : null), "getName"), "html", null, true);
                echo "
                        </span>
                    </div>
                </div>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "        <div id=\"death_logPagination\" class=\"force-center\"></div>

        <script type=\"text/javascript\">
            \$(document).ready(function () {
                callModulePage(
                        'death_log',
                        ";
            // line 53
            echo twig_escape_filter($this->env, (isset($context["death_log_pages"]) ? $context["death_log_pages"] : null), "html", null, true);
            echo ",
                        ";
            // line 54
            echo twig_escape_filter($this->env, (isset($context["death_log_page"]) ? $context["death_log_page"] : null), "html", null, true);
            echo "
                );
            });
        </script>
    ";
        } else {
            // line 59
            echo "        <div class='force-center'><em>No death log.</em></div>
    ";
        }
        // line 61
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "mod/death_log.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 61,  147 => 54,  135 => 47,  119 => 40,  112 => 36,  89 => 24,  86 => 23,  80 => 21,  74 => 19,  68 => 17,  65 => 16,  56 => 13,  47 => 10,  29 => 4,  22 => 2,  102 => 39,  98 => 38,  78 => 26,  66 => 20,  62 => 15,  58 => 17,  33 => 7,  137 => 51,  124 => 43,  114 => 39,  104 => 34,  94 => 27,  85 => 25,  54 => 16,  41 => 8,  36 => 8,  32 => 5,  21 => 2,  155 => 59,  149 => 59,  143 => 53,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 22,  76 => 27,  72 => 23,  46 => 11,  35 => 6,  28 => 5,  87 => 32,  81 => 24,  71 => 18,  63 => 17,  43 => 14,  59 => 14,  45 => 11,  39 => 10,  40 => 8,  27 => 4,  24 => 3,  109 => 43,  103 => 32,  100 => 32,  96 => 38,  77 => 20,  73 => 21,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 7,  30 => 7,  61 => 17,  49 => 17,  37 => 8,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 50,  129 => 59,  123 => 41,  120 => 54,  116 => 45,  108 => 36,  105 => 32,  101 => 31,  95 => 45,  92 => 29,  88 => 30,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 12,  50 => 11,  44 => 9,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
