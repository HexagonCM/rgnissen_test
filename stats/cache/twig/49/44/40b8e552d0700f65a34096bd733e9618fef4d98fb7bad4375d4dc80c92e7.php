<?php

/* overview/deaths.tpl */
class __TwigTemplate_494440b8e552d0700f65a34096bd733e9618fef4d98fb7bad4375d4dc80c92e7 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row grid\">
    <div class=\"col-xs-4\">
        <span class=\"label label-success no-img\">
            ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "total"), "html", null, true);
        echo "
        </span> ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("total_kills"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-success no-img\">
            ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "deaths"), "html", null, true);
        echo "
        </span> ";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("total_deaths"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-success grid-img\">
            ";
        // line 14
        echo $this->env->getExtension('Statistics')->staticCall("Material", "getMaterialImg", array(0 => $this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "top_weapon"), 1, array(), "array"), 1 => 32, 2 => null, 3 => true));
        echo "
        </span> ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("best_weapon"), "html", null, true);
        echo "
    </div>
</div>
<div class=\"row grid\">
    <div class=\"col-xs-4\">
        <span class=\"label label-success no-img\">
            ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "pve"), "html", null, true);
        echo "
        </span> ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("pve"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("kills"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-success grid-img\">
            ";
        // line 26
        echo $this->env->getExtension('Statistics')->staticCall("Entity", "getEntityImg", array(0 => $this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_dangerous"), 1, array(), "array"), 1 => 32, 2 => null, 3 => true));
        echo "
        </span> ";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("most_dangerous"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-success grid-img\">
            ";
        // line 31
        echo $this->env->getExtension('Statistics')->staticCall("Entity", "getEntityImg", array(0 => $this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_killed_mob"), 1, array(), "array"), 1 => 32, 2 => null, 3 => true));
        echo "
        </span> ";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("most_killed"), "html", null, true);
        echo "
    </div>
</div>
<div class=\"row grid\">
    <div class=\"col-xs-4\">
        <span class=\"label label-success no-img\">
            ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "pvp"), "html", null, true);
        echo "
        </span> ";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("pvp"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("kills"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-danger grid-img\">
            ";
        // line 43
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "top_killer"), 1, array(), "array"), "getName") != "none")) {
            // line 44
            echo "                <a href=\"?page=player&id=";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "top_killer"), 1, array(), "array"), "getPlayerId"), "html", null, true);
            echo "\">
                    ";
            // line 45
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "top_killer"), 1, array(), "array"), "getPlayerHead", array(0 => 32, 1 => null, 2 => true), "method");
            echo "
                </a>
            ";
        } else {
            // line 48
            echo "                ";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "top_killer"), 1, array(), "array"), "getPlayerHead");
            echo "
            ";
        }
        // line 50
        echo "        </span> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("most_kills"), "html", null, true);
        echo "
    </div>
    <div class=\"col-xs-4\">
        <span class=\"label label-danger grid-img\">
            ";
        // line 54
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_killed_player"), 1, array(), "array"), "getName") != "none")) {
            // line 55
            echo "                <a href=\"?page=player&id=";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_killed_player"), 1, array(), "array"), "getPlayerId"), "html", null, true);
            echo "\">
                    ";
            // line 56
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_killed_player"), 1, array(), "array"), "getPlayerHead", array(0 => 32, 1 => null, 2 => true), "method");
            echo "
                </a>
            ";
        } else {
            // line 59
            echo "                ";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["death_stats"]) ? $context["death_stats"] : null), "most_killed_player"), 1, array(), "array"), "getPlayerHead");
            echo "
            ";
        }
        // line 61
        echo "        </span> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("most_deaths"), "html", null, true);
        echo "
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "overview/deaths.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 61,  149 => 59,  143 => 56,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 31,  76 => 27,  72 => 26,  46 => 14,  35 => 9,  28 => 5,  87 => 32,  81 => 37,  71 => 30,  63 => 22,  43 => 14,  59 => 21,  45 => 16,  39 => 10,  40 => 8,  27 => 3,  24 => 4,  109 => 43,  103 => 32,  100 => 39,  96 => 38,  77 => 24,  73 => 32,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 17,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 45,  108 => 47,  105 => 46,  101 => 48,  95 => 45,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 15,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
