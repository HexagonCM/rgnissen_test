<?php

/* overview/headbar.tpl */
class __TwigTemplate_1615875548923b720e79e552dc178b46ba6120539404d7b825d4f5fe254fdecd extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-3\">
    <div class=\"well\">
        <h1><i class=\"fa fa-group fa-lg\"></i></h1>

        <h3>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats"]) ? $context["player_stats"] : null), "online"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->env->getExtension('Statistics')->translate("online")), "html", null, true);
        echo "</h3>
    </div>
</div>
<div class=\"col-md-3\">
    <div class=\"well\">
        <h1><i class=\"fa fa-pencil fa-lg\"></i></h1>

        <h3>";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats"]) ? $context["player_stats"] : null), "tracked"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("tracked"), "html", null, true);
        echo "</h3>
    </div>
</div>
<div class=\"col-md-3\">
    <div class=\"well\">
        <h1><i class=\"fa fa-times-circle fa-lg\"></i></h1>

        <h3>";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player_stats"]) ? $context["player_stats"] : null), "killed"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("killed"), "html", null, true);
        echo "</h3>
    </div>
</div>
<div class=\"col-md-3\">
    <div class=\"well\">
        <h1><i class=\"fa fa-arrow-up fa-lg\"></i></h1>

        <h3>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "uptime_perc"), "html", null, true);
        echo "% ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("uptime"), "html", null, true);
        echo "</h3>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "overview/headbar.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 26,  49 => 19,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 35,  79 => 34,  75 => 33,  67 => 27,  64 => 26,  60 => 25,  53 => 20,  50 => 19,  44 => 16,  34 => 8,  31 => 7,  26 => 4,  23 => 3,  19 => 1,);
    }
}
