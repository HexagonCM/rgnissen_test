<?php

/* mod/total_items.tpl */
class __TwigTemplate_167f108bfd99bbfb51a511442600756c495fc03bb3f724c96b95bfc6fa435253 extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table table-striped table-bordered table-vcenter\">
    <thead>
    <tr>
        <th class=\"sort-button ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 1, array(), "array"), "html", null, true);
        echo "\" data-type=\"1\" data-sort=\"asc\">
            ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("item_type"), "html", null, true);
        echo "
        </th>
        <th class=\"sort-button ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 2, array(), "array"), "html", null, true);
        echo "\" data-type=\"2\" data-sort=\"asc\">
            ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("picked_up"), "html", null, true);
        echo "
        </th>
        <th class=\"sort-button ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 3, array(), "array"), "html", null, true);
        echo "\" data-type=\"3\" data-sort=\"asc\">
            ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("dropped"), "html", null, true);
        echo "
        </th>
    </tr>
    </thead>
    <tbody class=\"content\">
    ";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["item_list"]) ? $context["item_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 17
            echo "        <tr>
            <td>
                ";
            // line 19
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "getImage", array(0 => 32, 1 => "img-thumbnail"), "method");
            echo "
                ";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "getName"), "html", null, true);
            echo "
            </td>
            <td>
                ";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->env->getExtension('Statistics')->staticCall("TotalItem", "countAllOfType", array(0 => "picked_up", 1 => (isset($context["item"]) ? $context["item"] : null)))), "html", null, true);
            echo "
            </td>
            <td>
                ";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->formatFNumber($this->env->getExtension('Statistics')->staticCall("TotalItem", "countAllOfType", array(0 => "dropped", 1 => (isset($context["item"]) ? $context["item"] : null)))), "html", null, true);
            echo "
            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </tbody>
</table>
<div id=\"item_listPagination\" class=\"force-center\"></div>

<script type=\"text/javascript\">
    \$(document).ready(function () {
        callModulePage(
            'item_list',
            ";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item_list"]) ? $context["item_list"] : null), "getPages"), "html", null, true);
        echo ",
            ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item_list"]) ? $context["item_list"] : null), "getPage"), "html", null, true);
        echo "
        );
    });
</script>";
    }

    public function getTemplateName()
    {
        return "mod/total_items.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 39,  98 => 38,  78 => 26,  66 => 20,  62 => 19,  58 => 17,  33 => 7,  137 => 51,  124 => 43,  114 => 39,  104 => 34,  94 => 30,  85 => 25,  54 => 16,  41 => 10,  36 => 8,  32 => 7,  21 => 2,  155 => 61,  149 => 59,  143 => 56,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 31,  76 => 27,  72 => 23,  46 => 11,  35 => 9,  28 => 5,  87 => 32,  81 => 24,  71 => 30,  63 => 17,  43 => 14,  59 => 16,  45 => 11,  39 => 10,  40 => 8,  27 => 4,  24 => 4,  109 => 43,  103 => 32,  100 => 32,  96 => 38,  77 => 23,  73 => 21,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 17,  37 => 8,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 50,  129 => 59,  123 => 55,  120 => 54,  116 => 45,  108 => 36,  105 => 46,  101 => 48,  95 => 45,  92 => 29,  88 => 30,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 13,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
