<?php

/* overview/blocks.tpl */
class __TwigTemplate_487519598b97d5b25faba9ab4dfdcc672b1ef58cd0aa357d411e04584d18b29c extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table no-border statbox-table-small\">
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-plus\"></i> ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "placed"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("placed"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                ";
        // line 13
        echo $this->env->getExtension('Statistics')->staticCall("Material", "getMaterialImg", array(0 => $this->getAttribute($this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "most_placed"), 1, array(), "array"), 1 => 16, 2 => null, 3 => true));
        echo "
                ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "most_placed"), 0, array(), "array"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("top_placed"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-minus\"></i> ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "destroyed"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("broken"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                ";
        // line 30
        echo $this->env->getExtension('Statistics')->staticCall("Material", "getMaterialImg", array(0 => $this->getAttribute($this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "most_destroyed"), 1, array(), "array"), 1 => 16, 2 => null, 3 => true));
        echo "
                ";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["block_stats"]) ? $context["block_stats"] : null), "most_destroyed"), 0, array(), "array"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("top_broken"), "html", null, true);
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "overview/blocks.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 34,  71 => 30,  63 => 25,  43 => 14,  59 => 24,  45 => 16,  39 => 13,  40 => 8,  27 => 3,  24 => 2,  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 32,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 17,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 14,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
