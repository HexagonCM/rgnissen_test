<?php

/* mod/players.tpl */
class __TwigTemplate_f31ba2515a3a5af6cbd5beb99774184b715c711c6473652562477353a427ca0b extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["players"]) ? $context["players"] : null)) == 0)) {
            // line 2
            echo "    <div class='force-center'><em>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("no_players_tracked"), "html", null, true);
            echo "</em></div>
";
        } else {
            // line 4
            echo "    <table class=\"table table-striped table-bordered table-hover table-vcenter\" id=\"playersTable\">
        <thead>
        <tr>
            <th class=\"sort-button ";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 1, array(), "array"), "html", null, true);
            echo "\" data-type=\"1\" data-sort=\"desc\">
                ";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("name"), "html", null, true);
            echo "
            </th>
            <th class=\"sort-button ";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 2, array(), "array"), "html", null, true);
            echo "\" data-type=\"2\" data-sort=\"desc\">
                ";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("last_seen"), "html", null, true);
            echo "
            </th>
            <th class=\"sort-button ";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 3, array(), "array"), "html", null, true);
            echo "\" data-type=\"3\" data-sort=\"desc\">
                ";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("date_joined"), "html", null, true);
            echo "
            </th>
            <th class=\"sort-button ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 4, array(), "array"), "html", null, true);
            echo "\" data-type=\"4\" data-sort=\"desc\">
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("playtime"), "html", null, true);
            echo "
            </th>
        </tr>
        ";
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["players"]) ? $context["players"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
                // line 21
                echo "            <tr>
                <td>
                    <a href=\"?page=player&id=";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlayerId"), "html", null, true);
                echo "\">
                        ";
                // line 24
                echo $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlayerHead", array(0 => 32, 1 => "img-thumbnail"), "method");
                echo "
                        ";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getName"), "html", null, true);
                echo "
                    </a>
                </td>
                <td>
                    ";
                // line 29
                if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLoginTime") != 0)) {
                    // line 30
                    echo "                        ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getLoginTime")), "html", null, true);
                    echo "
                    ";
                } else {
                    // line 32
                    echo "                        <em>never</em>
                    ";
                }
                // line 34
                echo "                </td>
                <td>
                    ";
                // line 36
                echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->dateFilter($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getFirstLogin")), "html", null, true);
                echo "
                </td>
                <td>
                    ";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Util"]) ? $context["Util"] : null), "formatSeconds", array(0 => $this->env->getExtension('Statistics')->fTimestamp($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "getPlaytime")), 1 => false), "method"), "html", null, true);
                echo "
                </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "    </table>
    <div id=\"playersPagination\" class=\"force-center\"></div>

    <script type=\"text/javascript\">
        \$(document).ready(function () {
            callModulePage(
                    'players',
                    ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["players"]) ? $context["players"] : null), "getPages"), "html", null, true);
            echo ",
                    ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["players"]) ? $context["players"] : null), "getPage"), "html", null, true);
            echo "
            );
        });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "mod/players.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 51,  124 => 43,  114 => 39,  104 => 34,  94 => 30,  85 => 25,  54 => 14,  41 => 10,  36 => 8,  32 => 7,  21 => 2,  155 => 61,  149 => 59,  143 => 56,  138 => 55,  128 => 50,  122 => 48,  111 => 44,  83 => 31,  76 => 27,  72 => 26,  46 => 14,  35 => 9,  28 => 5,  87 => 32,  81 => 24,  71 => 30,  63 => 17,  43 => 14,  59 => 16,  45 => 11,  39 => 10,  40 => 8,  27 => 4,  24 => 4,  109 => 43,  103 => 32,  100 => 32,  96 => 38,  77 => 23,  73 => 21,  69 => 20,  57 => 22,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 17,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 54,  133 => 50,  129 => 59,  123 => 55,  120 => 54,  116 => 45,  108 => 36,  105 => 46,  101 => 48,  95 => 45,  92 => 29,  88 => 39,  82 => 25,  79 => 34,  75 => 31,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 13,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
