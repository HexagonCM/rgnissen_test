<?php

/* overview/server_stats.tpl */
class __TwigTemplate_a7d3a55a57510b8e8dffd7d5924fe93ef4d9a5ebcb9f020ed993a53abb2087ef extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table no-border statbox-table-small\">
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-bell\"></i> ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "startup"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("startup"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-warning\">
                <i class=\"fa fa-lock\"></i> ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "shutdown"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("shutdown"), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-danger\">
                <i class=\"fa fa-calendar\"></i> ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "cur_uptime"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 24
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->env->getExtension('Statistics')->translate("uptime")), "html", null, true);
        echo "</td>
    </tr>
    <tr>
        <td>
            <span class=\"label label-info\">
                <i class=\"fa fa-bullhorn\"></i> ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["serverstats"]) ? $context["serverstats"] : null), "playtime"), "html", null, true);
        echo "
            </span>
        </td>
        <td>";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("gameplay"), "html", null, true);
        echo "</td>
    </tr>
</table>";
    }

    public function getTemplateName()
    {
        return "overview/server_stats.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 24,  45 => 16,  39 => 13,  40 => 8,  27 => 3,  24 => 2,  109 => 35,  103 => 32,  100 => 31,  96 => 29,  77 => 24,  73 => 32,  69 => 20,  57 => 16,  48 => 13,  42 => 10,  38 => 9,  30 => 7,  61 => 17,  49 => 19,  37 => 12,  25 => 5,  222 => 109,  219 => 108,  209 => 105,  205 => 104,  195 => 96,  192 => 95,  186 => 92,  179 => 87,  176 => 86,  170 => 83,  160 => 75,  157 => 74,  150 => 70,  146 => 69,  136 => 61,  133 => 60,  129 => 59,  123 => 55,  120 => 54,  116 => 53,  108 => 47,  105 => 46,  101 => 45,  95 => 41,  92 => 40,  88 => 39,  82 => 25,  79 => 34,  75 => 33,  67 => 29,  64 => 18,  60 => 25,  53 => 21,  50 => 14,  44 => 16,  34 => 8,  31 => 8,  26 => 4,  23 => 3,  19 => 1,);
    }
}
