<?php

/* mod/player_blocks.tpl */
class __TwigTemplate_32959c2e8f980aca5eec2fb6e8946d202f0902f5fba21e653eb712f18c45a45c extends Statistics_Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table table-striped table-bordered table-vcenter\">
    <thead>
    <tr>
        <th class=\"sort-button ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 1, array(), "array"), "html", null, true);
        echo "\" data-type=\"1\" data-sort=\"asc\">
            ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("block_type"), "html", null, true);
        echo "
        </th>
        <th class=\"sort-button ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 2, array(), "array"), "html", null, true);
        echo "\" data-type=\"2\" data-sort=\"asc\">
            ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("destroyed"), "html", null, true);
        echo "
        </th>
        <th class=\"sort-button ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sort"]) ? $context["sort"] : null), 3, array(), "array"), "html", null, true);
        echo "\" data-type=\"3\" data-sort=\"asc\">
            ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Statistics')->translate("placed"), "html", null, true);
        echo "
        </th>
    </tr>
    </thead>
    <tbody class=\"content\">
    ";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["block_list"]) ? $context["block_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tb"]) {
            // line 17
            echo "        ";
            $context["block"] = $this->getAttribute((isset($context["tb"]) ? $context["tb"] : null), "createMaterial", array(), "method");
            // line 18
            echo "        <tr>
            <td>
                ";
            // line 20
            echo $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getImage", array(0 => 32, 1 => "img-thumbnail"), "method");
            echo "
                ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : null), "getName"), "html", null, true);
            echo "
            </td>
            <td>
                ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tb"]) ? $context["tb"] : null), "prepareDestroyed"), "html", null, true);
            echo "
            </td>
            <td>
                ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tb"]) ? $context["tb"] : null), "preparePlaced"), "html", null, true);
            echo "
            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "    </tbody>
</table>
<div id=\"block_listPagination\" class=\"force-center\"></div>

<script type=\"text/javascript\">
    \$(document).ready(function () {
        callModulePage(
            'block_list',
            ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block_list"]) ? $context["block_list"] : null), "getPages"), "html", null, true);
        echo ",
            ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block_list"]) ? $context["block_list"] : null), "getPage"), "html", null, true);
        echo "
        );
    });
</script>";
    }

    public function getTemplateName()
    {
        return "mod/player_blocks.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 40,  101 => 39,  91 => 31,  81 => 27,  75 => 24,  69 => 21,  65 => 20,  61 => 18,  58 => 17,  54 => 16,  46 => 11,  37 => 8,  33 => 7,  24 => 4,  1053 => 582,  1046 => 577,  1043 => 576,  1032 => 567,  1029 => 566,  1016 => 555,  1009 => 553,  1001 => 550,  997 => 549,  994 => 548,  989 => 547,  981 => 541,  975 => 538,  970 => 537,  968 => 536,  960 => 531,  952 => 526,  947 => 525,  945 => 524,  939 => 520,  936 => 519,  930 => 516,  925 => 515,  923 => 514,  915 => 509,  907 => 504,  902 => 503,  900 => 502,  894 => 498,  892 => 497,  887 => 495,  880 => 491,  870 => 483,  864 => 480,  859 => 479,  857 => 478,  849 => 473,  840 => 467,  836 => 466,  831 => 465,  829 => 464,  823 => 460,  820 => 459,  814 => 456,  809 => 455,  807 => 454,  799 => 449,  790 => 443,  786 => 442,  781 => 441,  779 => 440,  773 => 436,  771 => 435,  766 => 433,  758 => 428,  750 => 423,  742 => 418,  726 => 405,  720 => 401,  716 => 399,  710 => 397,  708 => 396,  700 => 391,  694 => 387,  690 => 385,  684 => 383,  682 => 382,  674 => 377,  659 => 365,  650 => 359,  641 => 353,  630 => 345,  621 => 339,  612 => 333,  601 => 325,  592 => 319,  583 => 313,  572 => 305,  563 => 299,  554 => 293,  536 => 278,  528 => 273,  520 => 268,  512 => 263,  504 => 258,  496 => 253,  488 => 248,  477 => 239,  473 => 237,  468 => 235,  463 => 234,  461 => 233,  453 => 228,  447 => 224,  443 => 222,  438 => 220,  433 => 219,  431 => 218,  423 => 213,  412 => 204,  408 => 202,  403 => 200,  398 => 199,  396 => 198,  388 => 193,  382 => 189,  378 => 187,  373 => 185,  368 => 184,  366 => 183,  358 => 178,  340 => 163,  334 => 161,  331 => 160,  328 => 159,  325 => 158,  322 => 157,  320 => 156,  311 => 150,  305 => 148,  302 => 147,  299 => 146,  296 => 145,  293 => 144,  291 => 143,  284 => 138,  278 => 136,  270 => 134,  268 => 133,  262 => 131,  259 => 130,  256 => 129,  253 => 128,  250 => 127,  248 => 126,  241 => 121,  235 => 119,  227 => 117,  225 => 116,  219 => 114,  216 => 113,  213 => 112,  210 => 111,  207 => 110,  205 => 109,  199 => 105,  193 => 102,  186 => 98,  180 => 95,  174 => 92,  168 => 89,  164 => 87,  162 => 86,  156 => 82,  150 => 80,  148 => 79,  140 => 74,  131 => 68,  122 => 62,  112 => 55,  104 => 50,  98 => 46,  93 => 43,  89 => 41,  85 => 39,  83 => 38,  79 => 36,  76 => 35,  68 => 29,  66 => 28,  59 => 24,  55 => 23,  51 => 21,  45 => 17,  42 => 10,  36 => 12,  30 => 8,  28 => 5,  21 => 2,  19 => 1,);
    }
}
